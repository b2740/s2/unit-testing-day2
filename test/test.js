const { factorial, div_check } = require("../src/util.js");
const { expect, assert } = require("chai");

describe("Test Factorials", () => {
  it("Test that 5! is 120", () => {
    const product = factorial(5);
    expect(product).to.equal(120);
  });

  it("Test that 1! is 1", () => {
    const product = factorial(1);
    assert.equal(product, 1);
  });

  // activity.1
  it("Assert that 0! is 1", () => {
    const product = factorial(0);
    assert.equal(product, 1);
  });

  it("Expect that 4! is 24", () => {
    const product = factorial(4);
    expect(product).to.equal(24);
  });

  it("Assert that 10! is 3628800", () => {
    const product = factorial(10);
    assert.equal(product, 3628800);
  });

  it("Test negative factorial is undefined", () => {
    const product = factorial(-1);
    expect(product).to.equal(undefined);
  });

  it("Should return an error if the input is a non-integer value", () => {
    assert.throws(
      () => {
        factorial("1.2");
      },
      Error,
      "Non-integer input"
    );
  });
});

//activity.3 different interpretation
describe("Test div_check", () => {
  it("Test that 105 is divisible by 5", () => {
    const result = div_check(105, 5);
    assert.equal(result, true);
  });

  it("Test that 14 is divisible by 7", () => {
    const result = div_check(14, 7);
    assert.equal(result, true);
  });

  it("Test that 0 is divisible by both 5 and 7", () => {
    assert.equal(div_check(0, 5), true);
    assert.equal(div_check(0, 7), true);
  });

  it("Test that 22 is NOT divisible by 5 or 7", () => {
    assert.equal(div_check(22, 5), false);
    assert.equal(div_check(22, 7), false);
  });
});
