const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");

chai.use(http);

describe("API Test Suite", () => {
  it("Test API Get people is running", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
        done();
      });
  });

  it("Test API get people returns 200", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("Test API post person returns 400 if no NAME property", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        alias: "Mikasa",
        age: 27,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  //activity
  describe("ACTIVITY: test POST endpoint", () => {
    it("should be running/responding", (done) => {
      chai
        .request("http://localhost:5001")
        .post("/person")
        .type("json")
        .send({
          name: "Johnny Depp",
          alias: "JD",
          age: 44,
        })
        .end((err, res) => {
          expect(res).to.not.equal(undefined);
          done();
        });
    });

    it("should encounter error if there is no alias in the send body", (done) => {
      chai
        .request("http://localhost:5001")
        .post("/person")
        .type("json")
        .send({
          name: "Johnny Depp",
          age: 44,
        })
        .end((err, res) => {
          expect(res.status).to.equal(400);
          done();
        });
    });

    it("should encounter error if there is no age in the send body", (done) => {
      chai
        .request("http://localhost:5001")
        .post("/person")
        .type("json")
        .send({
          name: "Johnny Depp",
          alias: "JD",
        })
        .end((err, res) => {
          expect(res.status).to.equal(400);
          done();
        });
    });
  });
});
