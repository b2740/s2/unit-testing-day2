const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
const API_ORIGIN = "http://localhost:5001";

chai.use(http);

// npm run dev && npm run capstone.
describe("Capstone - /currency", function () {
  it("[1] post /currency should be running", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .then(function (res) {
        expect(res).to.not.equal(undefined);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[2] post /currency returns status 400 if 'name' is missing", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[3] post /currency returns status 400 if 'name' is not a string", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: 150,
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[4] post /currency returns status 400 if 'name' is empty string", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: "",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[5] post /currency returns status 400 if 'ex' is missing", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: "United States Dollar",
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[6] post /currency returns status 400 if 'ex' is not an object", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: "United States Dollar",
        ex: "she's cold",
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[7] post /currency returns status 400 if 'ex' is an empty object", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: "United States Dollar",
        ex: {},
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[8] post /currency returns status 400 if 'alias' is missing", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        name: "United States Dollar",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[9] post /currency returns status 400 if 'alias' is not a string", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: false,
        name: "United States Dollar",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[10] post /currency returns status 400 if 'alias' an empty string", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "",
        name: "United States Dollar",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[11] post /currency returns status 400 if all fields are complete but there is a duplicate 'alias'", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "dollars", //already exists
        name: "Pera pera",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(400);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });

  it("[12] post /currency returns status 200 if all fields are complete and there is no duplicate", function (done) {
    chai
      .request(API_ORIGIN)
      .post("/currency")
      .type("json")
      .send({
        alias: "jaxstones",
        name: "Pera pera",
        ex: {
          peso: 50.73,
          won: 1187.24,
          yen: 108.63,
          yuan: 7.03,
        },
      })
      .then(function (res) {
        expect(res.status).to.equal(200);
      })
      .catch(function (err) {
        throw err;
      })
      .then(done, done);
  });
});
