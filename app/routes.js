const { names, exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/people", (req, res) => {
    return res.send({
      people: names,
    });
  });

  app.post("/person", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Bad Request: missing required parameter NAME",
      });
    }

    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Bad Request: NAME has to be string.",
      });
    }

    if (!req.body.hasOwnProperty("age")) {
      return res.status(400).send({
        error: "Bad Rquest: missing required parameter AGE",
      });
    }

    if (typeof req.body.age !== "number") {
      return res.status(400).send({
        error: "Bad Request: AGE has to be a number",
      });
    }

    if (!req.body.hasOwnProperty("alias")) {
      return res.status(400).send({
        error: "Bad Request: missing required parameter ALIAS",
      });
    }

    return res.status(200).send({
      success: "No error",
    });
  });

  app.post("/currency", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Bad Request: missing required parameter NAME",
      });
    }

    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Bad Request: NAME must be of type string",
      });
    }

    if (req.body.name.length === 0) {
      return res.status(400).send({
        error: "Bad Request: NAME must not be empty",
      });
    }

    if (!req.body.hasOwnProperty("ex")) {
      return res.status(400).send({
        error: "Bad Request: missing required parameter EX",
      });
    }

    if (typeof req.body.ex !== "object") {
      return res.status(400).send({
        error: "Bad Request: EX must be an object",
      });
    }

    let obj = req.body.ex;
    if (
      Object.keys(obj).length === 0 &&
      Object.getPrototypeOf(obj) === Object.prototype
    ) {
      return res.status(400).send({
        error: "Bad Request: EX must be a non empty object",
      });
    }

    if (!req.body.hasOwnProperty("alias")) {
      return res.status(400).send({
        error: "Bad Request: missing required parameter ALIAS",
      });
    }

    if (typeof req.body.alias !== "string") {
      return res.status(400).send({
        error: "Bad Request: ALIAS must be of type 'string'",
      });
    }

    if (req.body.alias.length === 0) {
      return res.status(400).send({
        error: "Bad Request: ALIAS must not be an empty string",
      });
    }

    for (currency in exchangeRates) {
      if (exchangeRates[currency].alias === req.body.alias) {
        return res.status(400).send({
          error: "Bad Request: ALIAS already exists",
        });
      }
    }

    return res.send({
      success: true,
    });
  });
};
